﻿using UnityEngine;

public class TileObject : MonoBehaviour {

    public GameObject spriteObj; // The sprite's GameObject. Used for replacing the sprite.

    private float health;
    private float healthMax;
    private bool isBusy = false;
    private Mob interactingWith = null;
    private Vector2 gridLocation;
    private float interPercent;
    private bool interactable = false;


    // GET/SET METHODS

    public float Health
    {
        get { return health; }
        set { health = value; }
    }

    public float HealthMax
    {
        get { return healthMax; }
        set
        {
            healthMax = value;
            if (health < healthMax) { health = healthMax; }
        }
    }

    public bool IsBusy
    {
        get { return isBusy; }
    }

    public Mob InteractingWith
    {
        get { return interactingWith; }
    }

    public Vector2 GridLocation
    {
        get { return gridLocation; }
        set { gridLocation = value; }
    }

    public float InterPercent
    {
        get { return interPercent; }
        set { interPercent = value; }
    }

    public bool Interactable
    {
        get { return interactable; }
        set { interactable = value; }
    }


    // FUNCTIONALITY METHODS

    void Awake()
    {
        healthMax = Globals.tileObjDefaultHealthMax;
        health = healthMax;
        int x = (int)transform.position.x;
        int y = (int)transform.position.y;
        gridLocation = new Vector2(x, y);
    }

    // Override this with every class that inherits TileObject.
    public virtual void Interaction()
    {

    }

    // Set who the TileObject is currently trying to interact with.
    public void InteractWith(Mob mob)
    {
        isBusy = true;
        interactingWith = mob;
    }

    // Stop interacting if the TileObject is currently doing so.
    public void StopInteraction()
    {
        isBusy = false;
        interactingWith = null;
        interPercent = 0;
    }

    // Receive a given amount of damage. Kill the TO if its health goes below zero.
    public void TakeDamage(float dmg)
    {
        if (dmg <= health)
        {
            health -= dmg;
        }
        else
        {
            health = 0;
            Die();
        }
    }

    // Called when the TileObject dies for whatever reason.
    public void Die()
    {
        InteractingWith.StopInteraction();
        Destroy(gameObject);
    }
}