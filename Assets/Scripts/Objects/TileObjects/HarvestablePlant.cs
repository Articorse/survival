﻿using UnityEngine;

public class HarvestablePlant : TileObject
{

    public Sprite ripeSpr;
    public Sprite emptySpr;
    public string productName;
    public float productHealth;
    public int productHealthMax;
    public Sprite productSprite;
    public int productNutrition;
    public bool productConsumable;


    private float ripeness;
    private int ripenessMax;
    private float ripenessRate;
    private int ripenessThreshold;

    public float Ripeness
    {
        get { return ripeness; }
    }

    public int RipenessThreshold
    {
        get { return ripenessThreshold; }
    }

    void Awake()
    {
        HealthMax = Globals.tileObjDefaultHealthMax;
        Health = HealthMax;
        int x = (int)transform.position.x;
        int y = (int)transform.position.y;
        GridLocation = new Vector2(x, y);
        ripenessMax = Globals.harvDefaultRipenessMax;
        ripenessRate = Globals.harvDefaultRipenessRate;
        ripeness = 0;
        ripenessThreshold = Globals.harvDefaultRipenessThreshold;
    }

    void FixedUpdate()
    {
        Ripen();
        Interaction();
    }

    public void Ripen()
    {
        if (ripeness < 0)
        {
            ripeness = 0;
        }
        ripeness += ripenessRate * Time.deltaTime;
        if (ripeness > ripenessMax)
        {
            ripeness = ripenessMax;
        }
        if (ripeness > ripenessThreshold)
        {
            SpriteRenderer spr = spriteObj.GetComponent<SpriteRenderer>();
            spr.sprite = ripeSpr;
            Interactable = true;
        }
        else
        {
            SpriteRenderer spr = spriteObj.GetComponent<SpriteRenderer>();
            spr.sprite = emptySpr;
            Interactable = false;
        }
    }

    public override void Interaction()
    {
        if (IsBusy && InteractingWith != null)
        {
            int x = (int)InteractingWith.transform.position.x;
            int y = (int)InteractingWith.transform.position.y;
            if (Mathf.Abs(x - GridLocation.x) + Mathf.Abs(y - GridLocation.y) == 1)
            {
                InteractingWith.IsInteracting = true;
                InteractingWith.pBar.gameObject.SetActive(true);
                InteractingWith.pBar.Percent = InterPercent;
                if (InterPercent < 100)
                {
                    InterPercent += InteractingWith.IntRate * Time.deltaTime;
                }
                else
                {
                    Food f = new Food(productName, productHealthMax, productSprite, productConsumable, productNutrition);
                    InteractingWith.AddToInventory(f);
                    ripeness -= ripenessThreshold;
                    InteractingWith.StopInteraction();
                }
            }
            else
            {
                InteractingWith.pBar.gameObject.SetActive(false);
                InteractingWith.IsInteracting = false;
            }
        }
    }
}