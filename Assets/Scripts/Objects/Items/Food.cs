﻿using UnityEngine;

public class Food : Item {

    private int nutrition;

    public int Nutrition
    {
        get { return nutrition; }
    }

    public Food(string _name) : base(_name)
    {
        nutrition = Globals.itemDefaultNutrition;
    }

    public Food(string _name, int _healthMax, Sprite _sprite, bool _consumable, int _nutrition) : base(_name, _healthMax, _sprite, _consumable)
    {
        nutrition = _nutrition;
    }
}