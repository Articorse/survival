﻿using UnityEngine;

public class Item {

    public string name;
    public bool consumable;
    public float health;
    public int healthMax;
    public Sprite sprite;
    public bool locked = false;

    public Item(string _name)
    {
        name = _name;
        healthMax = Globals.itemDefaultHealthMax;
        health = healthMax;
        sprite = null;
        consumable = false;
    }

    public Item(string _name, int _healthMax, Sprite _sprite, bool _consumable)
    {
        Init(_name, _healthMax, _sprite, _consumable);
    }

    public void Init(string _name)
    {
        name = _name;
        healthMax = Globals.itemDefaultHealthMax;
        health = healthMax;
        sprite = null;
        consumable = false;
    }

    public void Init(string _name, int _healthMax, Sprite _sprite, bool _consumable)
    {
        name = _name;
        healthMax = _healthMax;
        health = healthMax;
        sprite = _sprite;
        consumable = _consumable;
    }
}