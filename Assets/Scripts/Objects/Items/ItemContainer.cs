﻿using UnityEngine;

public class ItemContainer : MonoBehaviour {

    private Item contents;

    public Item Contents
    {
        get { return contents; }
    }

    public void Create(Item i)
    {
        contents = i;
        if (contents != null)
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.sprite = contents.sprite;
        } else
        {
            Debug.Log("Assigned item is null.");
            Destroy(gameObject);
        }
    }
}