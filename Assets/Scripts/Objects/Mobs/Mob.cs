﻿using System.Collections.Generic;
using UnityEngine;

public class Mob : MonoBehaviour
{
    // GRAPHICS
    // The four directional sprites for this mob.
    public Sprite frontSpr;
    public Sprite backSpr;
    public Sprite rightSpr;
    public Sprite leftSpr;

    public GameObject spriteObj; // The sprite's GameObject. Used for replacing the sprite.
    private Vector3 lastPosition; // Used to determine the direction the mob is currently moving in.


    // ACTIONS
    public ProgressBar pBar; // This mob's personal progress bar for interactions.
    private bool isInteracting = false; // Whether the mob is currently ordered to interact with a TileObject.
    private TileObject interactingWith = null; // The TileObject the mob is currently interacting with.
    private float intRate;  // The rate at which this mob interacts with TileObjects.
    private bool droppingItem; // Signals that the mob is in the process of dropping an item somewhere.
    private Item itemToBeDropped; // The item the mob is currently trying to drop.
    private Vector2 itemDropDestination; // Where the item should be dropped.


    // PATHFINDING
    private TerrainBuilder tb;
    private PathFinder pathFinder;
    private IList<Vector2> path; // The mob's current movement path.
    private int pathPoint = 0; // How many more points are left on the current movement path.
    private bool move = false; // Whether the mob should be moving to a destination right now.
    private Vector2 curLoc; // The location of this mob, or where it's currently going.
    public GameObject moveMarker; // The indicator that shows where the mob is currently moving.


    // NEEDS
    private float hunger;
    private float hungerDet;
    private float hungerMax;


    // STATS
    private float moveSpeed;


    // MISC
    public List<Item> inventory = new List<Item>();
    public GameObject itemContainer;


    //  GET/SET METHODS
    public float Hunger
    {
        get { return hunger; }
        set { hunger = value; }
    }

    public float IntRate
    {
        get { return intRate; }
    }

    public Vector2 CurLoc
    {
        get { return curLoc; }
    }

    public bool IsInteracting
    {
        get { return isInteracting; }
        set { isInteracting = value; }
    }

    // FUNCTIONALITY METHODS

    void Awake()
    {
        tb = GameObject.FindGameObjectWithTag(Tags.canvas).GetComponent<TerrainBuilder>();
        moveSpeed = Globals.mobDefaultMoveSpeed;
        pathFinder = GetComponent<PathFinder>();
        curLoc = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        tb.OccupyTile(curLoc);
        lastPosition = transform.position;
        hungerMax = Globals.mobDefaultHungerMax;
        hunger = hungerMax;
        hungerDet = Globals.mobDefaultHungerDet;
        intRate = Globals.mobDefaultInteractionRate;
    }

    private void FixedUpdate()
    {
        SpriteDirection();
        Movement();
        NeedsDeterioration();
        DroppingItem();
    }

    // The gradual deterioration of the mob's needs.
    private void NeedsDeterioration()
    {
        if (hunger < 0) { hunger = 0; }
        else if (hunger > hungerMax) { hunger = hungerMax; }
        else { hunger -= hungerDet * Time.deltaTime; }
    }

    // Moves the mob along the current path.
    private void Movement()
    {
        if (move && path != null)
        {
            moveMarker.transform.SetParent(null);
            moveMarker.transform.position = new Vector3(path[0].x, path[0].y, moveMarker.transform.position.z);
            moveMarker.SetActive(true);
            float wt = 1;
            int pathX = Mathf.RoundToInt(path[pathPoint].x);
            int pathY = Mathf.RoundToInt(path[pathPoint].y);
            int mobX = Mathf.RoundToInt(transform.position.x);
            int mobY = Mathf.RoundToInt(transform.position.y);
            if (pathX != mobX && pathY != mobY)
            {
                TerrainTile t = null;
                if (tb.Terrain.TryGetValue(path[pathPoint], out t))
                {
                    wt = t.Weight * Globals.diagonalWeightModifier;
                }
            }
            else
            {
                TerrainTile t = null;
                if (tb.Terrain.TryGetValue(path[pathPoint], out t))
                {
                    wt = t.Weight;
                }
            }
            float speed = moveSpeed / wt;
            float step = Time.deltaTime * speed;
            transform.position = Vector2.MoveTowards(transform.position, path[pathPoint], step);

            if (transform.position.x == path[pathPoint].x && transform.position.y == path[pathPoint].y)
            {
                pathPoint--;
            }

            if (pathPoint < 0) { move = false; }
        }
        else
        {
            moveMarker.transform.SetParent(transform);
            moveMarker.SetActive(false);
        }
    }

    // Used to make a mob go to a given location.
    public void GoTo(Vector2 destination, bool stopPerviousInteraction = true)
    {
        if (stopPerviousInteraction)
        {
            StopInteraction();
        }
        Vector2 dest = tb.FindNearestFreeNode(destination);
        IList<Vector2> tPath = pathFinder.FindShortestPath(dest);
        if (tPath != null)
        {
            path = tPath;
            pathPoint = path.Count - 1;
            move = true;
            tb.FreeTile(curLoc);
            curLoc = path[0];
            tb.OccupyTile(curLoc);
        }
    }

    // Try to interact with the given TileObject.
    public void TryToInteract(TileObject obj)
    {
        StopInteraction();
        if (!obj.IsBusy && obj.Interactable)
        {
            List<Vector2> neighbors = tb.GetFreeDirectNeighbors(obj.GridLocation, transform);
            if (neighbors.Count > 0)
            {
                interactingWith = obj;
                obj.InteractWith(GetComponent<Mob>());
                IList<Vector2> p = new List<Vector2>();
                int dest = 0;
                bool goTo = true;
                for (int i = 0; i < neighbors.Count; i++)
                {
                    IList<Vector2> np = pathFinder.FindShortestPath(neighbors[i]);
                    if (np == null)
                    {
                        goTo = false;
                        break;
                    }
                    if (p.Count == 0 || (np.Count < p.Count))
                    {
                        p = np;
                        dest = i;
                    }
                }
                if (goTo)
                {
                    GoTo(neighbors[dest], false);
                }
            }
        }
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public void StopInteraction()
    {
        if (interactingWith != null)
        {
            interactingWith.StopInteraction();
        }
        isInteracting = false;
        interactingWith = null;
        pBar.gameObject.SetActive(false);
    }

    // Makes the mob's sprite face the direction it's moving, or down in stationary. Or, if it's currently interacting with something, makes it face its target.
    private void SpriteDirection()
    {
        Vector2 direction;
        if (!isInteracting)
        {
            direction = transform.position - lastPosition;
        }
        else
        {
            direction = interactingWith.GridLocation - (Vector2)transform.position;
            pBar.gameObject.SetActive(true);
        }
        SpriteRenderer spr = spriteObj.GetComponent<SpriteRenderer>();
        Vector2 localDirection = transform.InverseTransformDirection(direction);
        lastPosition = transform.position;
        if (localDirection == Vector2.zero)
        {
            spr.sprite = frontSpr;
        }
        else if (localDirection.x > 0)
        {
            spr.sprite = rightSpr;
        }
        else if (localDirection.x < 0)
        {
            spr.sprite = leftSpr;
        }
        else if (localDirection.y < 0)
        {
            spr.sprite = frontSpr;
        }
        else if (localDirection.y > 0)
        {
            spr.sprite = backSpr;
        }
    }

    public void ConsumeItem(int i)
    {
        if (inventory.Count > i - 1)
        {
            Food f = new Food("");
            if (inventory[i].GetType() == f.GetType())
            {
                Food c = (Food)inventory[i];
                Eat(c);
            }
        }
    }

    public void Eat(Food f)
    {
        if (inventory.Contains(f))
        {
            hunger += f.Nutrition;
            RemoveFromInventory(f);
        }
    }

    public void AddToInventory(Item i)
    {
        inventory.Add(i);
        GameObject c = GameObject.FindGameObjectWithTag(Tags.canvas);
        Selection s = c.GetComponent<Selection>();
        if (c.GetComponent<Selection>().DrawInventoryUI)
        {
            s.RedrawInvUI();
        }
    }

    public void RemoveFromInventory(Item i)
    {
        if (inventory.Contains(i))
        {
            inventory.Remove(i);
            GameObject c = GameObject.FindGameObjectWithTag(Tags.canvas);
            Selection s = c.GetComponent<Selection>();
            if (c.GetComponent<Selection>().DrawInventoryUI)
            {
                s.RedrawInvUI();
            }
        }
        else
        {
            Debug.Log("No such item in inventory.");
        }
    }

    public void DropItem(int i, Vector2 dropLoc)
    {
        if (inventory[i] == null || dropLoc.x < 0 || dropLoc.y < 0 || inventory[i].locked)
        {
            Debug.Log("Invalid input for DropItem.");
            return;
        }
        List<Vector2> neighbors = tb.GetFreeDirectNeighbors(dropLoc, transform);
        if (neighbors != null && neighbors.Count > 0)
        {
            Vector2 dest = tb.FindNearestFreeNode(dropLoc, true);
            if (dest.x > 0 && dest.y > 0)
            {
                droppingItem = true;
                itemToBeDropped = inventory[i];
                itemToBeDropped.locked = true;
                itemDropDestination = dropLoc;
                GoTo(dest);
            }
            else
            {
                Debug.Log("Invalid location for DropItem.");
            }
        }
    }

    private void DroppingItem()
    {
        if (droppingItem)
        {
            StopInteraction();
            if (itemToBeDropped != null && itemDropDestination.x > 0 && itemDropDestination.y > 0)
            {
                int x = (int)transform.position.x;
                int y = (int)transform.position.y;
                if (Mathf.Abs(x - itemDropDestination.x) + Mathf.Abs(y - itemDropDestination.y) == 1)
                {
                    GameObject iContainer = Instantiate(itemContainer, itemDropDestination, tb.transform.rotation, tb.transform);
                    iContainer.GetComponent<ItemContainer>().Create(itemToBeDropped);
                    RemoveFromInventory(itemToBeDropped);
                    itemToBeDropped = null;
                    itemDropDestination = new Vector2(-1, -1);
                    droppingItem = false;
                }
            }
            else
            {
                Debug.Log("Invalid item or drop location.");
            }
        }
    }
}