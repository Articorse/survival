﻿using System.Collections.Generic;
using UnityEngine;

public class TerrainBuilder : MonoBehaviour
{

    // MAP GENERATION VARIABLES
    public int height;                                  // The number of tiles on the Y axis.
    public int width;                                   // The number of tiles on the X axis.
    public GameObject emptyTerrain;                     // The empty tile prefab - set in editor.
    public GameObject passableTile;                     // The passable tile prefab - set in editor. Placeholder.
    public GameObject impassableTile;                   // The impassable tile prefab - set in editor. Placeholder.
    public System.Random r;                             // The randomization seed.
    public Transform terrainContainer;                  // The transform used as a parent for terrain tiles.

    // PATHFINDING
    private Dictionary<Vector2, TerrainTile> terrain;   // Allows access to all TerrainTiles through their coordinates.
    private HashSet<Vector2> passableTileLocs;          // A list of all the passable tiles on the map.
    private List<Vector2> occupiedTiles;                // A list of all tiles that already have a mob in them.
    private int FNFPLoops = 0;                          // The number of times FindNearestFreePoint() has recurred so far.
    private List<Vector2> FNFPCheckedNodes;             // The nodes FindNearestFreePoint() has gone through. Used to make sure it doesn't loop through the same nodes over and over.


    // GET/SET METHODS

    public Dictionary<Vector2, TerrainTile> Terrain
    {
        get { return terrain; }
    }

    public HashSet<Vector2> PassableTileLocs
    {
        get { return passableTileLocs; }
    }

    public void OccupyTile(Vector2 tile)
    {
        if (!occupiedTiles.Contains(tile))
        {
            occupiedTiles.Add(tile);
        }
    }

    public void FreeTile(Vector2 tile)
    {
        if (occupiedTiles.Contains(tile))
        {
            occupiedTiles.Remove(tile);
        }
    }

    public bool IsTileOccupied(Vector2 tile)
    {
        if (occupiedTiles.Contains(tile))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<Vector2> OccupiedTiles
    {
        get { return occupiedTiles; }
    }

    // FUNCTIONALITY METHODS

    // Generates the terrain over 7 passes of all tiles:
    // Pass 1. Creates empty tiles based on the assigned width and height.
    // Pass 2. Creates seed tiles for impassable terrain. The number of seed tiles is determined by the total number of tiles from the CalculateSeedTiles() function.
    // Pass 3. Grows the seed tiles by randomly making their neighbors(and their neighbors, etc.) impassable. The pass is performed from bottom to top, left to right.
    // Pass 4. Repeats 3, but does so from right ro left, top to bottom.
    // Pass 5. Repeats 3.
    // Pass 6. Removes single passable tiles surrounded by impassable tiles, and vice versa. !!! HAS PLACEHOLDER SCRIPT !!!
    // Pass 7. Enables the colliders of impassable tiles that have at least one passable neighbor. By default, colliders are disabled.
    void Awake()
    {
        r = new System.Random(Globals.randomSeed); // Sets the random seed from Globals.
        occupiedTiles = new List<Vector2>();
        FNFPCheckedNodes = new List<Vector2>();
        terrain = InitializeTerrain(); // Pass 1.
        int seedTiles = CalculateSeedTiles(width, height); // Calculates how many seed tiles should be on the map.

        // Adds a small random element to the number of seed tiles.
        int seedRandomization = r.Next(-Globals.seedVariability, Globals.seedVariability);
        seedTiles += seedTiles * seedRandomization / 100;

        // Pass 2. Technically, it could be more than a whole pass, or less than one.
        while (seedTiles > 0)
        {
            for (int y = 0; y <= height; y++)
            {
                if (seedTiles <= 0)
                {
                    break;
                }
                for (int x = 0; x <= width; x++)
                {
                    if (seedTiles <= 0)
                    {
                        break;
                    }
                    TerrainTile curTile = null;
                    if (terrain.TryGetValue(new Vector2(x, y), out curTile))
                    {
                        float rand = r.Next(0, 10000);
                        rand /= 10000;
                        if (rand <= Globals.firstScanProbability)
                        {
                            curTile.Passable = false;
                            seedTiles -= 1;
                        }
                    }
                    else
                    {
                        Debug.Log("Tile at " + x + ", " + y + " missing during TerrainBuilder Pass 2.");
                    }
                }
            }
        }

        // Pass 3.
        for (int y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++)
            {
                TerrainTile curTile = null;
                if (terrain.TryGetValue(new Vector2(x, y), out curTile))
                {
                    GrowImpassable(curTile);
                }
                else
                {
                    Debug.Log("Tile at " + x + ", " + y + " missing during TerrainBuilder Pass 3.");
                }
            }
        }

        // Pass 4.
        for (int x = width; x >= 0; x--)
        {
            for (int y = height - 1; y >= 0; y--)
            {
                TerrainTile curTile = null;
                if (terrain.TryGetValue(new Vector2(x, y), out curTile))
                {
                    GrowImpassable(curTile);
                }
                else
                {
                    Debug.Log("Tile at " + x + ", " + y + " missing during TerrainBuilder Pass 4.");
                }
            }
        }

        // Pass 5.
        for (int y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++)
            {
                TerrainTile curTile = null;
                if (terrain.TryGetValue(new Vector2(x, y), out curTile))
                {
                    GrowImpassable(curTile);
                }
                else
                {
                    Debug.Log("Tile at " + x + ", " + y + " missing during TerrainBuilder Pass 5.");
                }
            }
        }

        // Pass 6.
        for (int y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++)
            {
                TerrainTile curTile = null;
                if (terrain.TryGetValue(new Vector2(x, y), out curTile))
                {

                    // !!! CHANGE WHEN YOU ADD MORE TYPES OF TILES !!!
                    curTile.Weight = Globals.defaultTileWeight; // Assigns the weight of the tile.
                    // !!! CHANGE WHEN YOU ADD MORE TYPES OF TILES !!!

                    if (!HasNeighbor(curTile, 2))
                    {
                        curTile.Passable = !curTile.Passable;
                    }
                }
                else
                {
                    Debug.Log("Tile at " + x + ", " + y + " missing during TerrainBuilder Pass 6.");
                }
            }
        }

        // Pass 7.
        passableTileLocs = new HashSet<Vector2>();
        for (int y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++)
            {
                TerrainTile curTile = null;
                if (terrain.TryGetValue(new Vector2(x, y), out curTile))
                {
                    if (curTile.Passable)
                    {
                        Instantiate(passableTile, curTile.transform.position, curTile.transform.rotation, curTile.transform);
                        passableTileLocs.Add(curTile.GridPos);
                    }
                    else
                    {
                        GameObject impassTile = Instantiate(impassableTile, curTile.transform.position, curTile.transform.rotation, curTile.transform);
                        if (HasNeighbor(curTile, 0))
                        {
                            impassTile.GetComponent<PolygonCollider2D>().enabled = true;
                        }
                    }
                }
                else
                {
                    Debug.Log("Tile at " + x + ", " + y + " missing during TerrainBuilder Pass 7.");
                }
            }
        }
    }

    // Fills the terrain with empty tiles and returns a list of all of them based on X and Y coordinates for easy reference later.
    private Dictionary<Vector2, TerrainTile> InitializeTerrain()
    {
        Dictionary<Vector2, TerrainTile> ter = new Dictionary<Vector2, TerrainTile>();
        for (int x = 0; x <= width; x++)
        {
            for (int y = 0; y <= height; y++)
            {
                Vector2 curPos = new Vector2(x, y);
                GameObject curTileObj = Instantiate(emptyTerrain, curPos, transform.rotation, terrainContainer);
                curTileObj.name = "Tile " + curTileObj.transform.position.x + " " + curTileObj.transform.position.y;
                TerrainTile curTerrainTile = curTileObj.GetComponent<TerrainTile>();
                curTerrainTile.GridPos = curPos;
                ter.Add(curPos, curTerrainTile);
            }
        }
        return ter;
    }

    // Grows out the scattered singular impassable tiles on the map into larger randomized clusters of impassable tiles.
    private void GrowImpassable(TerrainTile curTile)
    {
        if (!curTile.Passable)
        {
            List<Vector2> neighborsLoc = CollectNeighborLocs(curTile);
            foreach (Vector2 neighborLoc in neighborsLoc)
            {
                float rand = r.Next(0, 100);
                rand /= 100;
                if (rand < Globals.secondScanProbability)
                {
                    TerrainTile t = null;
                    terrain.TryGetValue(new Vector2((int)neighborLoc.x, (int)neighborLoc.y), out t);
                    t.Passable = false;
                }
            }
        }
    }

    // Removes single passable tiles that are completely surrounded by impassable tiles, and vice versa.
    // Mode 0 checks for passable neighbors, Mode 1 checks for impassable, Mode 2 checks for neighbors that are the same as the given tile.
    private bool HasNeighbor(TerrainTile curTile, int mode)
    {
        if (mode < 0 || mode > 2)
        {
            Debug.Log("Invalid HasNeighbor() Mode.");
            return false;
        }
        List<Vector2> neighborsLoc = CollectNeighborLocs(curTile);
        foreach (Vector2 neighborLoc in neighborsLoc)
        {
            TerrainTile curNeighbor = null;
            terrain.TryGetValue(new Vector2((int)neighborLoc.x, (int)neighborLoc.y), out curNeighbor);
            if (curNeighbor.Passable && mode == 0 || !curNeighbor.Passable && mode == 1 || curTile.Passable == curNeighbor.Passable && mode == 2)
            {
                return true;
            }
        }
        return false;
    }

    // Collects all the locations of the given tile's neighbors into a list.
    private List<Vector2> CollectNeighborLocs(TerrainTile curTile)
    {
        List<Vector2> neighborsLoc = new List<Vector2>();
        int curX = (int)curTile.GridPos.x;
        int curY = (int)curTile.GridPos.y;
        if (curX - 1 >= 0)
        {
            neighborsLoc.Add(new Vector2(curX - 1, curY));
        }
        if (curX + 1 <= width)
        {
            neighborsLoc.Add(new Vector2(curX + 1, curY));
        }
        if (curY - 1 >= 0)
        {
            neighborsLoc.Add(new Vector2(curX, curY - 1));
        }
        if (curY + 1 <= height)
        {
            neighborsLoc.Add(new Vector2(curX, curY + 1));
        }
        return neighborsLoc;
    }

    // Calculates the number of seed impassable tiles for the map.
    public int CalculateSeedTiles(int width, int height)
    {
        int area = width * height;
        return (area / Globals.impassSeedDivider);
    }

    // Find the nearest unoccupied, pathable node of the given node.
    public Vector2 FindNearestFreeNode(Vector2 node, bool excludeCenter = false)
    {
        if (!FNFPCheckedNodes.Contains(node))
        {
            FNFPCheckedNodes.Add(node);
        }
        if (FNFPLoops < Globals.neighborRecursions)
        {
            if (!IsTileOccupied(node) && passableTileLocs.Contains(node) && !excludeCenter)
            {
                FNFPLoops = 0;
                FNFPCheckedNodes = new List<Vector2>();
                return node;
            }
            else
            {
                List<Vector2> neighbors = GetPathableNeighbors(node);
                foreach (Vector2 n in neighbors)
                {
                    if (!IsTileOccupied(n))
                    {
                        FNFPLoops = 0;
                        FNFPCheckedNodes = new List<Vector2>();
                        return n;
                    }
                }
                foreach (Vector2 n in neighbors)
                {
                    Vector2 p = FindNearestFreeNode(n);
                    FNFPLoops = 0;
                    FNFPCheckedNodes = new List<Vector2>();
                    return p;
                }
                FNFPLoops++;
            }
        }
        FNFPLoops = 0;
        FNFPCheckedNodes = new List<Vector2>();
        return new Vector2(-1, -1);
    }

    // Returns a list of all reachable, unoccupied neighbors of the given node.
    private List<Vector2> GetPathableNeighbors(Vector2 node)
    {
        List<Vector2> pNeighbors = new List<Vector2>();
        IList<Vector2> posSideNodes = new List<Vector2>() {
            new Vector2 (node.x + 1, node.y),
            new Vector2 (node.x - 1, node.y),
            new Vector2 (node.x, node.y + 1),
            new Vector2 (node.x, node.y - 1) };
        IList<Vector2> posDiagNodes = new List<Vector2>() {
            new Vector2(node.x + 1, node.y + 1),
            new Vector2(node.x + 1, node.y - 1),
            new Vector2(node.x - 1, node.y + 1),
            new Vector2(node.x - 1, node.y - 1)};
        foreach (Vector2 n in posSideNodes)
        {
            if (passableTileLocs.Contains(n) && !FNFPCheckedNodes.Contains(n))
            {
                pNeighbors.Add(n);
            }
        }
        foreach (Vector2 n in posDiagNodes)
        {
            if (passableTileLocs.Contains(n) && !FNFPCheckedNodes.Contains(n))
            {
                Vector2 dir = n - node;
                Vector2 nX = new Vector2(node.x + dir.x, node.y);
                Vector2 nY = new Vector2(node.x, node.y + dir.y);
                if (passableTileLocs.Contains(nX) || passableTileLocs.Contains(nY))
                {
                    pNeighbors.Add(n);
                }
            }
        }
        return pNeighbors;
    }

    // Returns a list of all non-diagonal, pathable neighbors of the given node.
    // Can exclude a given transform in case a mob is standing in a valid neighbor while trying to interact with a TileObject.
    public List<Vector2> GetFreeDirectNeighbors(Vector2 node, Transform userMob = null)
    {
        List<Vector2> neighbors = new List<Vector2>();
        IList<Vector2> posSideNodes = new List<Vector2>() {
            new Vector2 (node.x + 1, node.y),
            new Vector2 (node.x - 1, node.y),
            new Vector2 (node.x, node.y + 1),
            new Vector2 (node.x, node.y - 1) };
        foreach (Vector2 n in posSideNodes)
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(n, Vector2.zero);
            if (hitInfo.transform == null || hitInfo.transform == userMob)
            {
                neighbors.Add(n);
            }
        }
        return neighbors;
    }
}
