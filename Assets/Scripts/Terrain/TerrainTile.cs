﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTile : MonoBehaviour {

    private Vector2 gridPos; // The position of this tile on the grid.
    private bool passable = true; // Whether this tile can be walked on or not.
    private float weight; // The weight of this tile. Modifies how quickly mobs travel through it. Used for pathfinding.

    public bool Passable
    {
        get { return passable; }
        set { passable = value; }
    }

    public Vector2 GridPos
    {
        get { return gridPos; }
        set { gridPos = value; }
    }

    public float Weight
    {
        get { return weight; }
        set { weight = value; }
    }
}