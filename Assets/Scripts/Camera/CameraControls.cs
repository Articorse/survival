﻿using UnityEngine;

public class CameraControls : MonoBehaviour {

    private TerrainBuilder tb;              // The TerrainBuilder component of the map's grid. Used for centering the camera at the start.

    private float moveX = 0;                // The current velocity of the camera on the X axis.
    private float moveY = 0;                // The current velocity of the camera on the X axis.
    private float zoomLevel;                // The current level of zoom of the camera.
    private int zoomTotal;                  // The variability in zoom, centered so maximum zoom is 0.
    private bool midMouseDown;              // Whether the middle mouse button is being pressed down at the moment.
    private Vector2 mousePosStart;
    private Camera cam;

    void Awake()
    {
        zoomLevel = Camera.main.orthographicSize;
        tb = GameObject.FindGameObjectWithTag(Tags.canvas).GetComponent<TerrainBuilder>();
        transform.position = new Vector3(tb.width / 2, tb.height / 2, transform.position.z);
        zoomTotal = Globals.maxZoom - Globals.minZoom;
        mousePosStart = new Vector2();
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        MidMouseMoveCamera();
        WASDMoveCamera();
        Zoom();
    }

    // Drags the camera around by holding down the middle mouse button.
    private void MidMouseMoveCamera()
    {
        if (Input.GetKeyDown(KeyCode.Mouse2)) {
            Vector3 mousePosLocal = Input.mousePosition;
            mousePosStart = cam.ScreenToWorldPoint(mousePosLocal);
            midMouseDown = true;
        }

        if (Input.GetKeyUp(KeyCode.Mouse2))
        {
            midMouseDown = false;
        }

        if (midMouseDown)
        {
            ConstrainCamera(); // Makes sure the camera doesn't leave the bounds of the map.
            Vector3 mousePosLocal = Input.mousePosition;
            Vector2 mousePosMove = cam.ScreenToWorldPoint(mousePosLocal);
            Vector3 newPos = new Vector3(mousePosStart.x - mousePosMove.x, mousePosStart.y - mousePosMove.y, 0);
            transform.position += newPos;
        }
    }

    // Controls moving the camera with the direction keys.
    private void WASDMoveCamera()
    {
        ConstrainCamera(); // Makes sure the camera doesn't leave the bounds of the map.
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        float zoomPercent = (zoomLevel - Globals.minZoom) / zoomTotal; // The current percentage level of camera zoom. Zooming in slows down camera movement.

        // Calculates the movement speed of the camera based on a static base speed and the current level of zoom.
        float moveRate = Globals.cameraMoveRateZoomAdjust * zoomPercent;
        moveRate += Globals.cameraMoveRateBase;

        Vector3 orgMove = new Vector3(); // Original movement vector. Gets its size(speed) from moveRate.

        // Checks the horizontal axis(left and right) for input. If movement is detected, applies speed to the movement vector,
        // then gradually speeds the camera up until max speed is reached.
        // If not input is detected on the horizontal axis, applies a faster deceleration to the movement vector's X axis.
        if (h != 0)
        {
            if (h > 0)
            {
                orgMove = new Vector3(moveRate, 0, 0);
            }
            else if (h < 0)
            {
                orgMove = new Vector3(-moveRate, 0, 0);
            }
            moveX = Mathf.Lerp(moveX, orgMove.x, Time.deltaTime * Globals.cameraMoveAcceleration);
        }
        else
        {
            moveX = Mathf.Lerp(moveX, 0, Time.deltaTime * Globals.cameraMoveDeceleration);
        }

        // Same as above, but for the vertical axis.
        if (v != 0)
        {
            if (v > 0)
            {
                orgMove = new Vector3(orgMove.x, moveRate, 0);
            }
            else if (v < 0)
            {
                orgMove = new Vector3(orgMove.x, -moveRate, 0);
            }
            moveY = Mathf.Lerp(moveY, orgMove.y, Time.deltaTime * Globals.cameraMoveAcceleration);
        }
        else
        {
            moveY = Mathf.Lerp(moveY, 0, Time.deltaTime * Globals.cameraMoveDeceleration);
        }

        Vector3 finMove = new Vector3(moveX, moveY, 0); // Final movement vector, after applying Lerp.
        transform.Translate(finMove); // Moves the camera itself with the final movement vector.
    }

    // Returns the camera to inside the map if it tries to leave.
    private void ConstrainCamera()
    {
        if (transform.position.x < 0)
        {
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > tb.width)
        {
            transform.position = new Vector3(tb.width, transform.position.y, transform.position.z);
        }

        if (transform.position.y < 0)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
        else if (transform.position.y > tb.height)
        {
            transform.position = new Vector3(transform.position.x, tb.height, transform.position.z);
        }
    }

    // Controls zooming with the mousewheel.
    private void Zoom()
    {
        float z = Input.GetAxis("Mouse ScrollWheel");
        if (z > 0f)
        {
            zoomLevel -= zoomLevel * Globals.zoomRate;
        }
        else if (z < 0f)
        {
            zoomLevel += zoomLevel * Globals.zoomRate;
        }
        zoomLevel = Mathf.Clamp(zoomLevel, Globals.minZoom, Globals.maxZoom);
        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, zoomLevel, Time.deltaTime * Globals.zoomAcceleration);
    }
}
