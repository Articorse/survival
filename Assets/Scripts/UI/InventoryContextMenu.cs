﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryContextMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    public Selection sel;
    public GameObject invConMenuItem;

    private bool mouseOnElement = false;
    private List<GameObject> menuButtons;
    private Mob mob;

    void Awake()
    {
        menuButtons = new List<GameObject>();
    }

    public void DrawMenu(int itemIndex)
    {
        EraseMenu(false);
        mob = sel.selection[0].GetComponent<Mob>();
        if (mob != null && sel.selection.Count == 1)
        {
            Item item = mob.inventory[itemIndex];
            if (item.consumable)
            {
                AddButton(Tags.eat);
            }
            AddButton(Tags.drop);

        }
        else
        {
            Debug.Log("DrawMenu called without a single mob selected.");
        }
    }

    private void AddButton(string buttonAction)
    {
        GameObject button = Instantiate(invConMenuItem, transform);
        menuButtons.Add(button);
        InventoryContextButton iB = button.GetComponent<InventoryContextButton>();
        iB.iCM = GetComponent<InventoryContextMenu>();
        Text t = button.GetComponentInChildren<Text>();
        t.text = buttonAction;
        iB.buttonAction = buttonAction;
    }

    public void EraseMenu(bool deactivate = true)
    {
        foreach (GameObject obj in menuButtons)
        {
            Destroy(obj);
        }
        menuButtons = new List<GameObject>();
        if (deactivate)
        {
            gameObject.SetActive(false);
        }
    }

    public void ButtonAction(int itemIndex, string buttonAction)
    {
        switch (buttonAction)
        {
            case Tags.eat:
                mob.ConsumeItem(itemIndex);
                EraseMenu();
                return;
            case Tags.drop:
                sel.DropItem(itemIndex);
                EraseMenu();
                return;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOnElement = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOnElement = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!mouseOnElement)
        {
            EraseMenu();
        }
    }

}