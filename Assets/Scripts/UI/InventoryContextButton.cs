﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryContextButton : MonoBehaviour {

    public int itemIndex;
    public string buttonAction; // The action the button is used for. I.E. Eat, drop, give, etc.
    public InventoryContextMenu iCM;
    //private Mob mob;

    void Awake()
    {
        //GameObject canvas = GameObject.FindGameObjectWithTag(Tags.canvas);
        //Selection sel = canvas.GetComponent<Selection>();
        //mob = sel.selection[0].GetComponent<Mob>();
    }

    public void Press()
    {
        iCM.ButtonAction(itemIndex, buttonAction);
    }
}