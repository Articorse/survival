﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour {

    private SpriteRenderer bar; // The SpriteRenderer of the filling part of the progress bar.
    private float perWidth; // How many width units equal one percent fill.
    private float percent = 0; // The current fill percent.


    // GET/SET METHODS

    public float Percent
    {
        get { return percent; }
        set { percent = value; }
    }


    // FUNCTIONALITY METHODS

    void Awake()
    {
        Transform child = transform.GetChild(1);
        bar = child.GetComponent<SpriteRenderer>(); // Gets the progress bar's SpriteRenderer.
        perWidth = bar.size.x / 100; // Calculates how many width units equal one percent fill.
    }

    void Update()
    {
        if (percent < 0)
        {
            percent = 0;
        } else if ( percent > 100)
        {
            percent = 100;
        }
        bar.size = new Vector2(perWidth * percent, bar.size.y); // Updates how full the progress bar should be every frame.
    }
}