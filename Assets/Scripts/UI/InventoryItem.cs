﻿using UnityEngine;

public class InventoryItem : MonoBehaviour {

    public InventoryContextMenu iCM;
    public int itemIndex;

    public void Press()
    {
        iCM.gameObject.SetActive(true);
        iCM.DrawMenu(itemIndex);
        iCM.transform.parent.position = Input.mousePosition;
    }
}