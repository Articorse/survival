﻿using UnityEngine;
using UnityEngine.EventSystems;

// This class tells the Selection script to not count clicking on UI elements as clicking on the map.
public class UIElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    private Selection sel;

    void Awake()
    {
        GameObject canvas = GameObject.FindGameObjectWithTag(Tags.canvas);
        sel = canvas.GetComponent<Selection>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        sel.mouseOverUI = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        sel.mouseOverUI = false;
    }

    public void OnDestroy()
    {
        sel.mouseOverUI = false;
    }

    public void OnDisable()
    {
        sel.mouseOverUI = false;
    }
}