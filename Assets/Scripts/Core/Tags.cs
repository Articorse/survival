﻿public class Tags {

    public const string impassable = "Impassable";
    public const string passable = "Passable";
    public const string mob = "Mob";
    public const string playerMob = "Colonist";
    public const string canvas = "Canvas";
    public const string eat = "Eat";
    public const string drop = "Drop";
    public const string iCM = "InvConMenu";
}