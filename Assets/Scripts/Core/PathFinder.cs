﻿using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;

public class PathFinder : MonoBehaviour
{

    private TerrainBuilder tb; // Reference to the map's TerrainBuilder to get the tiles.
    private IDictionary<Vector2, Vector2> nodeParents = new Dictionary<Vector2, Vector2>(); // Populates with each node's parent during pathfinding.

    void Awake()
    {
        tb = GameObject.FindGameObjectWithTag(Tags.canvas).GetComponent<TerrainBuilder>();
    }

    // Use this function to find the path between the owner of the component and the given location.
    public IList<Vector2> FindShortestPath(Vector2 goal)
    {
        IList<Vector2> path = new List<Vector2>();
        int mobX = Mathf.RoundToInt(transform.position.x);
        int mobY = Mathf.RoundToInt(transform.position.y);
        Vector2 mobLoc = new Vector2(mobX, mobY);
        goal = FindShortestPathAStar(mobLoc, goal, "manhattan");
        if (!nodeParents.ContainsKey(mobLoc))
        {
            nodeParents.Add(new KeyValuePair<Vector2, Vector2>(mobLoc, mobLoc));
        }
        if (goal == mobLoc || !nodeParents.ContainsKey(nodeParents[goal]))
        {
            //No solution was found.
            return null;
        }

        Vector2 curr = goal;
        while (curr != mobLoc)
        {
            path.Add(curr);
            curr = nodeParents[curr];
        }

        foreach (Vector2 occTile in tb.OccupiedTiles)
        {
            if (path.Contains(occTile))
            {
                path.Remove(occTile);
            }
        }
        return path;
    }

    private Vector2 FindShortestPathAStar(Vector2 startPosition, Vector2 goalPosition, string heuristic)
    {
        float timeNow = Time.realtimeSinceStartup; // Used to check how long the function takes.
        IEnumerable<Vector2> validNodes = tb.PassableTileLocs; // Takes a reference to the list of passable tile positions.
        IDictionary<Vector2, float> totalScore = new Dictionary<Vector2, float>(); // Stores the total score for each node.
        IDictionary<Vector2, float> distanceFromStart = new Dictionary<Vector2, float>(); // Stores the distance of the given node from the start location of the pathfinding.

        // Initializes the totalScore and distanceFromStart lists by setting all values to maximum. This is so that even if one other value is found, it will be lower and will
        // therefore be put in the list instead.
        foreach (Vector2 vertex in validNodes)
        {
            totalScore.Add(new KeyValuePair<Vector2, float>(vertex, int.MaxValue));
            distanceFromStart.Add(new KeyValuePair<Vector2, float>(vertex, int.MaxValue));
        }

        totalScore[startPosition] = HeuristicCostEstimate(startPosition, goalPosition, heuristic); // Calculates the heuristic cost of the start position.
        distanceFromStart[startPosition] = 0; // Sets the distance from start to start to 0.
        HashSet<Vector2> exploredNodes = new HashSet<Vector2>(); // This list contains all nodes that have been explored so far.

        // Uses a priority queue to determine the order in which to explore nodes.
        SimplePriorityQueue<Vector2, float> priorityQueue = new SimplePriorityQueue<Vector2, float>();
        priorityQueue.Enqueue(startPosition, totalScore[startPosition]);
        while (priorityQueue.Count > 0)
        {
            // Get the node with the least distance from the start
            Vector2 curr = priorityQueue.Dequeue();

            // If our current node is the goal then stop
            if (curr == goalPosition)
            {
                print("A*" + heuristic + ": " + distanceFromStart[goalPosition]);
                print("A*" + heuristic + " time: " + (Time.realtimeSinceStartup - timeNow).ToString());
                return goalPosition;
            }

            //Otherwise, add it to our explored nodes set and look at its neighbors
            exploredNodes.Add(curr);

            IList<Vector2> neighbors = GetWalkableNodes(curr);

            foreach (Vector2 node in neighbors)
            {
                // If we've explored a neighbor, don't look at it again
                if (exploredNodes.Contains(node))
                {
                    continue;
                }

                // Get the distance so far, add it to the distance to the neighbor. Multiply the weight by the diagonal modifier for diagonal movement.
                TerrainTile tile;
                float currScore = 0;
                if (tb.Terrain.TryGetValue(node, out tile))
                {
                    if (node.x != curr.x && node.y != curr.y)
                    { currScore = distanceFromStart[curr] + (tile.Weight * Globals.diagonalWeightModifier); }
                    else
                    { currScore = distanceFromStart[curr] + tile.Weight; }
                }
                else
                {
                    Debug.Log("Couldn't get TerrainTile at (" + node.x + ", " + node.y + ") in PathFinder.");
                }

                // If we have not explored the neighbor, add it to the queue
                if (!priorityQueue.Contains(node))
                {
                    priorityQueue.Enqueue(node, totalScore[node]);
                }
                // If our distance to this neighbor is MORE than another calculated shortest path
                //    to this neighbor, skip it.
                else if (currScore >= distanceFromStart[node])
                {
                    continue;
                }

                // Otherwise if the score is LESS, we will set a new node parent and update the scores
                //    as our current best for the path so far.
                nodeParents[node] = curr;
                distanceFromStart[node] = currScore;
                totalScore[node] = distanceFromStart[node] + HeuristicCostEstimate(node, goalPosition, heuristic);
            }
        }

        return startPosition;
    }

    // Makes a list of all of the given node's neighbors.
    IList<Vector2> GetWalkableNodes(Vector2 curr)
    {
        IList<Vector2> walkableNodes = new List<Vector2>();

        IList<Vector2> sidePossibleNodes = new List<Vector2>() {
            new Vector2 (curr.x + 1, curr.y),
            new Vector2 (curr.x - 1, curr.y),
            new Vector2 (curr.x, curr.y + 1),
            new Vector2 (curr.x, curr.y - 1) };

        IList<Vector2> diagonalPossibleNodes = new List<Vector2>() {
            new Vector2 (curr.x + 1, curr.y + 1),
            new Vector2 (curr.x + 1, curr.y - 1),
            new Vector2 (curr.x - 1, curr.y + 1),
            new Vector2 (curr.x - 1, curr.y - 1) };

        foreach (Vector2 node in sidePossibleNodes)
        {
            if (CanMove(node))
            {
                walkableNodes.Add(node);
            }
        }

        if (CanMove(diagonalPossibleNodes[0]) && CanMove(sidePossibleNodes[0]) || CanMove(diagonalPossibleNodes[0]) && CanMove(sidePossibleNodes[2]))
        {
            walkableNodes.Add(diagonalPossibleNodes[0]);
        }
        if (CanMove(diagonalPossibleNodes[1]) && CanMove(sidePossibleNodes[0]) || CanMove(diagonalPossibleNodes[1]) && CanMove(sidePossibleNodes[3]))
        {
            walkableNodes.Add(diagonalPossibleNodes[1]);
        }
        if (CanMove(diagonalPossibleNodes[2]) && CanMove(sidePossibleNodes[1]) || CanMove(diagonalPossibleNodes[2]) && CanMove(sidePossibleNodes[2]))
        {
            walkableNodes.Add(diagonalPossibleNodes[2]);
        }
        if (CanMove(diagonalPossibleNodes[3]) && CanMove(sidePossibleNodes[1]) || CanMove(diagonalPossibleNodes[3]) && CanMove(sidePossibleNodes[3]))
        {
            walkableNodes.Add(diagonalPossibleNodes[3]);
        }

        return walkableNodes;
    }

    // Checks if the given node is passable.
    bool CanMove(Vector2 nextPosition)
    {
        if (tb.PassableTileLocs.Contains(nextPosition))
        {
            return true;
        }
        return false;
    }

    // Calculates the heuristic cost for pathfinding.
    int HeuristicCostEstimate(Vector2 node, Vector2 goal, string heuristic)
    {
        switch (heuristic)
        {
            case "euclidean":
                return EuclideanEstimate(node, goal);
            case "manhattan":
                return ManhattanEstimate(node, goal);
        }

        return -1;
    }

    int EuclideanEstimate(Vector2 node, Vector2 goal)
    {
        return (int)Mathf.Sqrt(Mathf.Pow(node.x - goal.x, 2) +
            Mathf.Pow(node.y - goal.y, 2));
    }

    int ManhattanEstimate(Vector2 node, Vector2 goal)
    {
        return (int)(Mathf.Abs(node.x - goal.x) +
            Mathf.Abs(node.y - goal.y));
    }
}