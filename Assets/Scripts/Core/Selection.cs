﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selection : MonoBehaviour
{

    public List<GameObject> selection;          // The currently selected object, if there is one.
    public GameObject selMarker;                // The box object that appears around whatever is selected.
    public GameObject selDragBox;               // The draggable selection box.
    public List<GameObject> selectionBoxes;     // The selection box images currently on the map.
    public Text selectionText;                  // The text in the UI for the selected object.
    public Image UIBackground;                  // The background for the selection text UI.
    public Camera cam;                          // The camera for the map.
    public List<Mob> playerMobs;                // A list of all player-controlled Mobs.
    public GameObject inventoryUI;              // The Inventory UI element.
    public GameObject inventoryListItem;        // The prefab for items appearing in the inventory UI.
    public GameObject inventoryContextMenuObj;  // The context menu container for inventory items.
    public GameObject selectionUI;              // The Selection UI element.
    public bool mouseOverUI = false;            // Whether the mouse is currently over a UI element or not.
    public bool waitingForDropLocation = false; // Whether the game is currently waiting for the player to click on a location to drop an item.

    private List<Mob> selectedMobs;             // The selected Mobs.
    private List<TileObject> selectedTOs;       // The selected TileObjects.
    private List<HarvestablePlant> selectedHPs; // The selected HarvestablePlants.
    private bool mouseDown = false;             // Whether the left mouse button is currently being held down. Used for selection.
    private Vector3 mousePos;                   // Local position of the mouse.
    private Vector3 mouseWorldPos;              // The world position of the mouse. Used for selection.
    private Vector3 mouseWorldPosB;             // The world position of the mouse while dragging a selection box.
    private Transform inventoryContentFitter;   // The Content Fitter Component in the Inventory UI element. inventory items are instantiated as its children.
    private bool lockedSelection = false;       // Locks the ability to select and issue orders.
    private InventoryContextMenu inventoryContextMenu;  // The context menu component of the container.
    private int itemIndexToBeDropped;           // The list index of the item a mob is trying to drop.


    public bool DrawInventoryUI
    {
        get
        {
            if (selectedMobs != null && selectedMobs.Count == 1) { return true; }
            else { return false; }
        }
    }

    void Awake()
    {
        // Initializes lists.
        playerMobs = new List<Mob>();
        selection = new List<GameObject>();
        selectedMobs = new List<Mob>();
        selectedTOs = new List<TileObject>();
        selectionBoxes = new List<GameObject>();

        // Finds all mobs that belong to the player and puts them in a list.
        GameObject[] mobs = GameObject.FindGameObjectsWithTag(Tags.playerMob);
        foreach (GameObject m in mobs)
        {
            playerMobs.Add(m.GetComponent<Mob>());
        }

        // Gets the Content Fitter of the Inventory UI element.
        try
        {
            inventoryContentFitter = inventoryUI.transform.GetChild(0);
        }
        catch (System.NullReferenceException e)
        {
            Debug.Log("Inventory UI element not initialized properly, missing Content Fitter.");
        }
        inventoryUI.SetActive(false);


        inventoryContextMenuObj.SetActive(true);
        inventoryContextMenu = inventoryContextMenuObj.GetComponent<InventoryContextMenu>();
        inventoryContextMenuObj.SetActive(false);
    }

    void Update()
    {
        // If there isn't a menu or something else currently taking up the attention of the mouse.
        if (!lockedSelection)
        {
            // LEFT CLICK
            if (Input.GetKeyDown(KeyCode.Mouse0)) // If the left mouse button is pressed down...
            {
                if (!mouseOverUI)
                {
                    if (inventoryContextMenu != null)
                    {
                        inventoryContextMenu.EraseMenu();
                    }
                    mousePos = Input.mousePosition;
                    mouseWorldPos = cam.ScreenToWorldPoint(mousePos);
                    mouseDown = true;
                }
                // Stores the mouse position when the button is pressed.
            }
            if (mouseDown)
            {
                // Stores the mouse position while the button is pressed.
                mousePos = Input.mousePosition;
                mouseWorldPosB = cam.ScreenToWorldPoint(mousePos);

                // If needed, makes the draggable selection box visible and draws it, offsetting its corners to line up with the mouse.
                if (Mathf.Abs(mouseWorldPos.x - mouseWorldPosB.x) >= Globals.selectionBoxTreshold || Mathf.Abs(mouseWorldPos.y - mouseWorldPosB.y) >= Globals.selectionBoxTreshold)
                {
                    float offsetX;
                    float offsetY;
                    if (mouseWorldPos.x > mouseWorldPosB.x)
                    {
                        offsetX = -Globals.selectionBoxOffset;
                    }
                    else
                    {
                        offsetX = Globals.selectionBoxOffset;
                    }
                    if (mouseWorldPos.y > mouseWorldPosB.y)
                    {
                        offsetY = -Globals.selectionBoxOffset;
                    }
                    else
                    {
                        offsetY = Globals.selectionBoxOffset;
                    }
                    Vector3 p1 = new Vector3(mouseWorldPos.x - offsetX, mouseWorldPos.y - offsetY, selDragBox.transform.position.z);
                    Vector3 p2 = new Vector3(mouseWorldPosB.x + offsetX, mouseWorldPosB.y + offsetY, selDragBox.transform.position.z);
                    selDragBox.SetActive(true);
                    StretchSprite(selDragBox, p1, p2);
                }
                else
                {
                    selDragBox.SetActive(false);
                }

                // If this was a single click:
                if (Input.GetKeyUp(KeyCode.Mouse0)) // Begins when the left mouse button is released.
                {
                    mouseDown = false;
                    selDragBox.SetActive(false);

                    if (Mathf.Abs(mouseWorldPos.x - mouseWorldPosB.x) < Globals.selectionBoxTreshold || Mathf.Abs(mouseWorldPos.y - mouseWorldPosB.y) < Globals.selectionBoxTreshold)
                    {
                        RaycastHit2D hitInfo = Physics2D.Raycast(mouseWorldPos, Vector2.zero); // Checks if there's a clickable object(a GameObject with a Collider) where the mouse is.
                        if (hitInfo.transform != null) // If there was an object - select it.
                        {
                            GameObject sel = hitInfo.transform.gameObject;
                            Select(sel);
                            if (selectedMobs.Count == 1)
                            {
                                RedrawInvUI();
                            }
                        }
                        else { Deselect(); } // If there was no object, deselect the current selection.
                    }

                    // If it was a dragged selection box:
                    else
                    {
                        // Get all the Collider2D components of everything in the dragged area.
                        Collider2D[] hits = Physics2D.OverlapAreaAll((Vector2)mouseWorldPos, (Vector2)mouseWorldPosB);

                        Deselect(); // Make sure there is nothing currently selected.
                        List<GameObject> sel = new List<GameObject>(); // Initialize the list of GameObjects that will be selected.

                        // First checks if there are any mobs in the selection. If there are, only selects the mobs.
                        bool mobInSelection = false;
                        foreach (Collider2D hit in hits)
                        {
                            GameObject obj = hit.gameObject;
                            Mob mob = obj.GetComponent<Mob>();
                            if (mob != null)
                            {
                                mobInSelection = true;
                                sel.Add(obj);
                            }
                        }
                        //  If there are no mobs, selects the rest of the objects.
                        if (!mobInSelection)
                        {
                            foreach (Collider2D hit in hits)
                            {
                                GameObject obj = hit.gameObject;
                                sel.Add(obj);
                            }
                        }
                        if (sel.Count > 0)
                        {
                            Select(sel, false);
                        }
                        if (selectedMobs.Count == 1)
                        {
                            RedrawInvUI();
                        }
                    }
                }
            }


            // RIGHT CLICK
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                if (!mouseOverUI)
                {
                    Vector3 mousePos = Input.mousePosition; // The current position of the mouse with relation to the screen, at the time of clicking.
                    Vector3 mouseWorldPos = cam.ScreenToWorldPoint(mousePos); // Same as above, but world position.
                    if (selectedMobs != null && selectedMobs.Count > 0) // If there is at least one selected Mob...
                    {
                        RaycastHit2D hitInfo = Physics2D.Raycast(mouseWorldPos, Vector2.zero); // Checks if there's a clickable object(a GameObject with a Collider) where the mouse is.
                        TileObject t = null;
                        if (hitInfo.transform != null)
                        {
                            t = hitInfo.transform.GetComponent<TileObject>();
                        }
                        if (selectedMobs.Count == 1 && t != null) // If there was an object, and there is only one mob selected, interact with the object.
                        {
                            selectedMobs[0].TryToInteract(t);
                        }
                        else // If more than one mob was selected, or if there was no object, or if the object was not a TileObject, treat the click as a move command.
                        {
                            int destX = Mathf.RoundToInt(mouseWorldPos.x);
                            int destY = Mathf.RoundToInt(mouseWorldPos.y);
                            Vector2 destination = new Vector2(destX, destY);
                            foreach (Mob m in selectedMobs)
                            {
                                if (m.CurLoc != destination)
                                {
                                    m.GoTo(destination);
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (waitingForDropLocation)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Vector3 mousePos = Input.mousePosition; // The current position of the mouse with relation to the screen, at the time of clicking.
                    Vector3 mouseWorldPos = cam.ScreenToWorldPoint(mousePos); // Same as above, but world position.
                    int x = Mathf.RoundToInt(mouseWorldPos.x);
                    int y = Mathf.RoundToInt(mouseWorldPos.y);
                    Vector2 loc = new Vector2(x, y);
                    selectedMobs[0].DropItem(itemIndexToBeDropped, loc);
                    itemIndexToBeDropped = 0;
                    waitingForDropLocation = false;
                    lockedSelection = false;
                }
            }
        }


        if (selection.Count > 0) // If there is currently an object selected...
        {
            foreach (GameObject obj in selection) // If there is a null object, deselect everything. FIX TO ONLY DESELECT THE NULL OBJECT!
            {
                if (obj == null)
                {
                    Deselect();
                    break;
                }
            }
            if (selection.Count == 1)
            {
                selectionText.text = selection[0].name; // Make the UI element display the object's name.
                if (selectedMobs.Count > 0) // If the current object is a Mob...
                {
                    // Add the Mob's stats to the UI element's text.
                    string newText = selectionText.text + "\nHunger: " + selectedMobs[0].Hunger.ToString();
                    selectionText.text = newText;
                }
                if (selectedTOs.Count > 0) // If the current object is a TileObject...
                {
                    // Add the TO's stats to the UI element's text.
                    string newText = selectionText.text + "\nHealth: " + selectedTOs[0].Health.ToString() + "/" + selectedTOs[0].HealthMax.ToString();
                    selectionText.text = newText;
                    if (selectedHPs.Count > 0) // If the TO is also a Harvestable Plant...
                    {
                        // Add the HP's stats to the UI element's text.
                        newText = "\nRipeness: " + selectedHPs[0].Ripeness.ToString();
                        selectionText.text += newText;
                        if (selectedHPs[0].Ripeness > selectedHPs[0].RipenessThreshold)
                        {
                            newText = "\nRipe!";
                            selectionText.text += newText;
                        }
                        else
                        {
                            newText = "\nNot ripe!";
                            selectionText.text += newText;
                        }
                    }
                }
            }
            else
            {
                selectionText.text = selection.Count + Globals.multipleSelectionText;
            }

        }
    }

    // Redraw the mob's inventory.
    public void RedrawInvUI()
    {
        EraseInvUI();
        inventoryUI.SetActive(true);
        for (int i = 0; i < selectedMobs[0].inventory.Count; i++)
        {
            GameObject invItem = Instantiate(inventoryListItem, inventoryContentFitter);
            invItem.GetComponent<InventoryItem>().itemIndex = i;
            invItem.GetComponent<InventoryItem>().iCM = inventoryContextMenuObj.GetComponent<InventoryContextMenu>();
            invItem.transform.GetChild(0).GetComponent<Text>().text = selectedMobs[0].inventory[i].name;
        }
    }

    // Selects the given object.
    private void Select(GameObject obj, bool clearPreviousSelection = true)
    {
        if (clearPreviousSelection)
        {
            Deselect();
        }
        selection.Add(obj);
        Mob mob = obj.GetComponent<Mob>();
        if (mob != null)
        {
            selectedMobs.Add(mob);
            Color mColor = mob.moveMarker.GetComponent<SpriteRenderer>().color;
            mob.moveMarker.GetComponent<SpriteRenderer>().color = new Color(mColor.r, mColor.g, mColor.b, mColor.a + Globals.selectedMoveMarkerHighlight);
        }
        TileObject tObject = obj.GetComponent<TileObject>();
        if (tObject != null)
        {
            selectedTOs.Add(tObject);
        }
        HarvestablePlant hPlant = obj.GetComponent<HarvestablePlant>();
        if (hPlant != null)
        {
            selectedHPs.Add(hPlant);
        }
        GameObject selBox = Instantiate(selMarker, selection[0].transform);
        selBox.transform.localPosition = Vector3.zero;
        selectionBoxes.Add(selBox);
        UIBackground.enabled = true;
    }

    // Selects the given objects.
    private void Select(List<GameObject> objs, bool clearPreviousSelection = true)
    {
        if (clearPreviousSelection)
        {
            Deselect();
        }
        foreach (GameObject obj in objs)
        {
            selection.Add(obj);
            Mob mob = obj.GetComponent<Mob>();
            if (mob != null)
            {
                selectedMobs.Add(mob);
                Color mColor = mob.moveMarker.GetComponent<SpriteRenderer>().color;
                mob.moveMarker.GetComponent<SpriteRenderer>().color = new Color(mColor.r, mColor.g, mColor.b, mColor.a + Globals.selectedMoveMarkerHighlight);
            }
            TileObject tObject = obj.GetComponent<TileObject>();
            if (tObject != null)
            {
                selectedTOs.Add(tObject);
            }
            HarvestablePlant hPlant = obj.GetComponent<HarvestablePlant>();
            if (hPlant != null)
            {
                selectedHPs.Add(hPlant);
            }
            GameObject selBox = Instantiate(selMarker, obj.transform);
            selBox.transform.localPosition = Vector3.zero;
            selectionBoxes.Add(selBox);
            UIBackground.enabled = true;
        }
    }

    // Deselects what was previously selected.
    private void Deselect()
    {
        foreach (Mob m in selectedMobs)
        {
            Color mColor = m.moveMarker.GetComponent<SpriteRenderer>().color;
            m.moveMarker.GetComponent<SpriteRenderer>().color = new Color(mColor.r, mColor.g, mColor.b, mColor.a - Globals.selectedMoveMarkerHighlight);
        }
        selection = new List<GameObject>();
        selectedMobs = new List<Mob>();
        selectedTOs = new List<TileObject>();
        selectedHPs = new List<HarvestablePlant>();
        foreach (GameObject sBox in selectionBoxes)
        {
            Destroy(sBox);
        }
        selectionBoxes = new List<GameObject>();
        selectionText.text = "";
        UIBackground.enabled = false;
        EraseInvUI();
    }

    private void EraseInvUI()
    {
        for (int i = 0; i < inventoryContentFitter.childCount; i++)
        {
            Destroy(inventoryContentFitter.GetChild(i).gameObject);
        }
        inventoryUI.SetActive(false);
    }

    public void DropItem(int i)
    {
        lockedSelection = true;
        waitingForDropLocation = true;
        itemIndexToBeDropped = i;
    }

    // Stretches a sprite from a GameObject that containst a SpriteRenderer between two points.
    public void StretchSprite(GameObject sprite, Vector3 pos1, Vector3 pos2)
    {
        SpriteRenderer sr = sprite.GetComponent<SpriteRenderer>();
        if (sr != null)
        {
            Vector3 centerPos = (pos1 + pos2) / 2f;
            sprite.transform.position = centerPos;
            sr.size = new Vector2(Mathf.Abs(pos1.x - pos2.x), Mathf.Abs(pos1.y - pos2.y));
        }
        else
        {
            Debug.Log("GameObject " + sprite.name + " has no SpriteRenderer Component.");
        }
    }
}