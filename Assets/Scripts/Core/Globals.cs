﻿public class Globals {

    // GLOBAL
    public const int   randomSeed = 1;                                  // The randomization seed.

    // TERRAIN GENERATION
    public const float firstScanProbability = 0.001f;                   // The default probability that a tile will be made impassable during the first scan of the terrain.
    public const float secondScanProbability = 0.49f;                   // The default probability that a tile will be made impassable during the subsequent scans of the terrain.
    public const int   impassSeedDivider = 150;                         // The number by which the total area of the map is divided to get the total number of impassable seed tiles.
    public const int   seedVariability = 10;                            // The percentage variability on the number of impassable seed tiles.

    // CAMERA
    public const int   maxZoom = 100;                                   // The farthest the camera can zoom out.
    public const int   minZoom = 5;                                     // The closest the camera can zoom in.
    public const float zoomRate = 0.1f;                                 // How quickly the camera zooms.
    public const float zoomAcceleration = 10;                           // How quickly the camera accelerates when zooming.
    public const float cameraMoveRateBase = 1;                          // How quickly the camera moves, regardless of zoom level.
    public const float cameraMoveRateZoomAdjust = 5;                    // How much the camera speed is affected by the level of zoom.
    public const float cameraMoveAcceleration = 2;                      // How quickly the camera accelerates when moving.
    public const float cameraMoveDeceleration = 7;                      // How quickly the camera decelerates when stopping.

    // MOBS
    public const float mobDefaultMoveSpeed = 5;                         // The default movement speed for all mobs.
    public const int   mobDefaultHungerMax = 100;                       // The default maximum hunger value for all mobs.
    public const float mobDefaultHungerDet = 1f;                        // The default deterioration rate for hunger for all mobs.
    public const float mobDefaultInteractionRate = 20f;                 // The default rate at which mobs interact with TileObjects.

    // TILE OBJECTS
    public const int   tileObjDefaultHealthMax = 100;                   // The default maximum health for TileObjects.

    // ITEMS
    public const int   itemDefaultHealthMax = 100;                      // The default maximum health for Items.
    public const int   itemDefaultNutrition = 20;                       // The default nutrition value for Food Items.

    // HARVESTABLE PLANTS
    public const int   harvDefaultRipenessMax = 100;                    // The default maximum ripeness a harvestable plant can reach.
    public const float harvDefaultRipenessRate = 10;                    // The default rate at which harvestable plants ripen.
    public const int   harvDefaultRipenessThreshold = 80;               // The threshold at which a harvestable plant is considered ripe.

    // PATHFINDING
    public const float defaultTileWeight = 1;                           // The default weight for all tiles.
    public const float diagonalWeightModifier = 1.414f;                 // The number diagonal neighbors' weight is multiplied by.
    public const int   neighborRecursions = 3;                          // How many times the FindNearestFreePoint() function should loop.

    // UI
    public const float selectionBoxTreshold = 0.2f;                     // The minimum size of a selection box.
    public const string multipleSelectionText = " Objects Selected";    // The text displayed in the UI if there is more than one object selected.
    public const float selectionBoxOffset = 0.5f;                       // The visual offset for the draggable selection box.
    public const float selectedMoveMarkerHighlight = 0.5f;              // How much the move marker of the selected mobs gets highlighted.
}   